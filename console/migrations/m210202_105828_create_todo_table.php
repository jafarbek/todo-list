<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%todo}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m210202_105828_create_todo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%todo}}', [
            'id' => $this->primaryKey(),
            'from_user' => $this->integer(),
            'to_user' => $this->integer(),
            'name' => $this->string(),
            'status' => $this->integer(),
        ]);

        // creates index for column `from_user`
        $this->createIndex(
            '{{%idx-todo-from_user}}',
            '{{%todo}}',
            'from_user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-todo-from_user}}',
            '{{%todo}}',
            'from_user',
            '{{%user}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `to_user`
        $this->createIndex(
            '{{%idx-todo-to_user}}',
            '{{%todo}}',
            'to_user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-todo-to_user}}',
            '{{%todo}}',
            'to_user',
            '{{%user}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-todo-from_user}}',
            '{{%todo}}'
        );

        // drops index for column `from_user`
        $this->dropIndex(
            '{{%idx-todo-from_user}}',
            '{{%todo}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-todo-to_user}}',
            '{{%todo}}'
        );

        // drops index for column `to_user`
        $this->dropIndex(
            '{{%idx-todo-to_user}}',
            '{{%todo}}'
        );

        $this->dropTable('{{%todo}}');
    }
}
