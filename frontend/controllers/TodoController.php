<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Todo;
use frontend\models\TodoSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TodoController implements the CRUD actions for Todo model.
 */
class TodoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','update','delete'],
                'rules' => [
                    [
                        'actions' => ['index','create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Todo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TodoSearch();
        $dataActive = $searchModel->search(Yii::$app->request->queryParams, Todo::STATUS_ACTIVE);
        $dataInProcess = $searchModel->search(Yii::$app->request->queryParams, Todo::STATUS_IN_PROCESS);
        $dataFinished = $searchModel->search(Yii::$app->request->queryParams, Todo::STATUS_FINISHED);

        return $this->render('index', [
            'dataActive' => $dataActive,
            'dataInProcess' => $dataInProcess,
            'dataFinished' => $dataFinished,
        ]);
    }

    public function actionAllGivenTasks()
    {
        $searchModel = new TodoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, false,true);

        return $this->render('all-given-tasks', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Todo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Todo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Todo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['all-given-tasks']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionStatusInProcess($id)
    {
        $model = $this->findModel($id);
        $model->status = Todo::STATUS_IN_PROCESS;
        if ($model->save()){
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionStatusFinished($id)
    {
        $model = $this->findModel($id);
        $model->status = Todo::STATUS_FINISHED;
        if ($model->save()){
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
    /**
     * Updates an existing Todo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Todo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Todo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Todo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Todo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
