<?php

namespace frontend\models;

use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "todo".
 *
 * @property int $id
 * @property int|null $from_user
 * @property int|null $to_user
 * @property string|null $name
 * @property int|null $status
 *
 * @property User $fromUser
 * @property User $toUser
 */
class Todo extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_IN_PROCESS = 2;
    const STATUS_FINISHED = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'todo';
    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from_user', 'to_user', 'status','name'], 'required'],
            [['from_user', 'to_user', 'status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['from_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_user' => 'id']],
            [['to_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_user' => 'id']],
        ];
    }

    public function getStatusLabels()
    {
        return [
          self::STATUS_ACTIVE=>Yii::t('app','Active'),
          self::STATUS_IN_PROCESS=>Yii::t('app','In the process'),
          self::STATUS_FINISHED=>Yii::t('app','Finished'),
        ];
    }

    public function getStatusLabel()
    {
        switch ($this->status){
            case self::STATUS_ACTIVE: return "<p class='label label-warning'>".Yii::t('app','Active')."<p>";
            case self::STATUS_IN_PROCESS: return "<p class='label label-primary'>".Yii::t('app','In the process')."<p>";
            case self::STATUS_FINISHED: return "<p class='label label-success'>".Yii::t('app','Finished')."<p>";
        }
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_user' => 'From User',
            'to_user' => 'To User',
            'name' => 'Todo name',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[FromUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user']);
    }

    /**
     * Gets query for [[ToUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user']);
    }

    public function getUsersList(){
        $users = User::find()->asArray()->all();
        return ArrayHelper::map($users, 'id', 'username');
    }
}
