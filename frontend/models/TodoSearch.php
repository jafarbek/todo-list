<?php

namespace frontend\models;

use frontend\models\Todo;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TodoSearch represents the model behind the search form of `frontend\models\Todo`.
 */
class TodoSearch extends Todo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'from_user', 'to_user', 'status'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $status=false, $fromUser=false)
    {
        if ($fromUser){
            $query = Todo::find()->where(['from_user'=>\Yii::$app->user->id]);
        }else{
            $query = Todo::find()->where(['status'=>$status, 'to_user'=>\Yii::$app->user->id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'from_user' => $this->from_user,
            'to_user' => $this->to_user,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->orderBy(['id'=>SORT_DESC]);

        return $dataProvider;
    }
}
