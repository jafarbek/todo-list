<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Todo */

$this->title = Yii::t('app', 'Create Todo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Todos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="todo-create">

    <h1><?= Html::encode($this->title) ?> <a class="pull-right btn btn-primary" href="<?=\yii\helpers\Url::to(['todo/all-given-tasks'])?>"><?=Yii::t('app','All given tasks')?></a> </h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
