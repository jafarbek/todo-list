<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Todo */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="row">

    <div class="col-md-6">
        <?= $form->field($model, 'to_user')->dropDownList($model->getUsersList(), ['prompt' => Yii::t('app', 'Please select user...')]) ?>
        <?= $form->field($model, 'from_user')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>

    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'name')->textarea(['maxlength' => true]) ?>
        <?= $form->field($model, 'status')->hiddenInput(['value' => $model::STATUS_ACTIVE])->label(false) ?>
    </div>


</div>
<div class="todo-form">
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
