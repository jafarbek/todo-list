<?php

use frontend\models\Todo;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TodoSearch */
/* @var $dataActive yii\data\ActiveDataProvider */
/* @var $dataInProcess yii\data\ActiveDataProvider */
/* @var $dataFinished yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Todos');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .todo-column{
        border: solid gray 1px;
        margin: 2px;
    }
</style>
<div class="row">
    <div class="col-md-4 todo-column">
        <div class="todo-index">

            <h1><?= "<p class='label label-warning'>".Yii::t('app','Active tasks')."<p>" ?></h1>


            <?= GridView::widget([
                'dataProvider' => $dataActive,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute'=>'from_user',
                        'value' => function($model){
                            return $model->fromUser->username;
                        }
                    ],
//            'to_user',
                    'name',

                    [
                        'class' => ActionColumn::class,
                        'template' => '{view}',
                        'headerOptions' => ['style'=>'width:15%; text-align:center'],
                        'contentOptions' => ['style'=>'text-align:center'],
                        'header' => Yii::t('app', 'Submit to process'),
                        'buttons' => [
                            'view' => function ($url, $model) {
                                if ($model->status == Todo::STATUS_ACTIVE)
                                    return \yii\bootstrap\Html::a('<span class="glyphicon glyphicon-ok"></span>', ['status-in-process', 'id' => $model->id], [
                                        'title' => Yii::t('app', 'View'),
                                        'class' => 'btn btn-info'
                                    ]);
                            },
                        ],
                    ],
                ],
            ]); ?>


        </div>

    </div>
    <div class="col-md-4 todo-column">
        <div class="todo-index">

            <h1><?= "<p class='label label-primary'>".Yii::t('app','Tasks in process ')."<p>" ?></h1>


            <?= GridView::widget([
                'dataProvider' => $dataInProcess,
//        'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute'=>'from_user',
                        'value' => function($model){
                            return $model->fromUser->username;
                        }
                    ],
                    'name',
                    [
                        'class' => ActionColumn::class,
                        'template' => '{view}',
                        'header' => Yii::t('app', 'Finish task button'),
                        'headerOptions' => ['style'=>'width:15%; text-align:center'],
                        'contentOptions' => ['style'=>'text-align:center'],
                        'buttons' => [
                            'view' => function ($url, $model) {
                                if ($model->status == Todo::STATUS_IN_PROCESS)
                                    return \yii\bootstrap\Html::a('<span class="glyphicon glyphicon-ok"></span>', ['status-finished', 'id' => $model->id], [
                                        'title' => Yii::t('app', 'View'),
                                        'class' => 'btn btn-success'
                                    ]);
                            },
                        ],
                    ],
                ],
            ]); ?>


        </div>

    </div>
    <div class="col-md-3 todo-column">
        <div class="todo-index">
            <h1><?= "<p class='label label-success'>".Yii::t('app','Finished tasks')."<p>" ?></h1>

            <?= GridView::widget([
                'dataProvider' => $dataFinished,
//        'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute'=>'from_user',
                        'value' => function($model){
                            return $model->fromUser->username;
                        }
                    ],
                    'name',
                ],
            ]); ?>


        </div>

    </div>
</div>
